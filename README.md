# Buildings (ReactJS)

This is a ReactJS practice project. The idea of the project is to create a collection of buildings and adds their info on cards, with pictures, descriptions etc. 

You may see and edit the cards and info about buildigs. Right now it uses json.db fake BE, sometimes later will move it on Java.

(In development right now)

See the tutorial and more info about the project: https://handsonreact.com/docs/

![Buildings and projects gallery](./public/assets/projectspage_on_react.png)

## Run the app
To run it, you would need Node.js installed in your computer.
- install the latest node.js
- git clone the project `git clone https://gitlab.com/java-remote-ee27-course/buildings-reactjs.git`
- go inside the project folder: buildings-reactjs
- install dependencies for react: `npm i`
- run `npm start`
- open cmd.exe (or terminal) inside the project folder: buildings-reactjs
- run `run npm api` to start the json.db fake backend
- navigate to page: http://localhost:3000

 
