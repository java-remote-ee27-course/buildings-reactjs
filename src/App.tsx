import React from 'react';
import ProjectsPage from './projects/ProjectsPage';
import './App.css';


function App() {

  return (
    <div>
      <ProjectsPage />
    </div>
  );

}

export default App;
