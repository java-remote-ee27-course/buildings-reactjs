import React, { useState, useEffect } from 'react';
import ProjectList from './ProjectList';
import { Project } from './Project';
import { projectAPI } from './projectAPI';

function ProjectsPage(){
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<string | undefined>(undefined);
    const [projects, setProjects] = useState<Project[]>([]);
    const [currentPage, setCurrentPage] = useState(1);

    const saveProject = (project: Project) =>{
        let updatedProjects = projects.map((p: Project) => {
            return p.id === project.id ? project : p;
        });
        setProjects(updatedProjects);
    };

    const handleMoreClick = () => {
        setCurrentPage((currentPage) => currentPage + 1);
    }

    useEffect(() => {
        async function loadProjects() {
            setLoading(true);
            try{
                const data = await projectAPI.get(currentPage);
                setError('');
                setProjects(data);
                if(currentPage === 1){
                    setProjects(data)
                } else {
                    setProjects((project) => [...project, ...data]);
                }
            } catch (e){
                if(e instanceof Error) {
                    setError(e.message);
                    setLoading(false);
                }                
            } finally {
                setLoading(false);
            }
        }
        loadProjects();
    }, [currentPage]);

    return(
        <>
            <h1>Projects</h1>
            { error && (
                <div className="row">
                    <div className="custom-error">
                        <section>
                            <p>
                                <span className="icon-alert"></span>
                                &nbsp;&nbsp;
                                {error}
                            </p>
                        </section>
                    </div>
                </div>
            )}


            <ProjectList onSave={saveProject} projects={projects} />
            {!loading && !error && (
                <div className="row">
                    <div className="col-sm-12">
                        <div>
                            <button className="button" onClick={handleMoreClick}>
                                Load more projects...
                            </button>
                        </div>
                    </div>
                </div>
            )}
            { loading && (
                <div className="center-page">
                <span className="spinner primary"></span>
                <p>Loading...</p>
           
                </div> 
            )}
        </>
    );
}

export default ProjectsPage;