import React, { SyntheticEvent, useState } from 'react';
import { Project } from './Project';

interface ProjectFormProps{
    project:Project;
    onCancel: () => void;
    onSave: (project: Project) => void;
}

function ProjectForm({project: initialProject, onSave, onCancel}:ProjectFormProps) {
    const [project, setProject] = useState(initialProject);

    const handleSubmit = (event:SyntheticEvent) => {
        event.preventDefault();
        onSave(project);
        //onSave(new Project({name: "Project updated" }));
    }

    const handleChange = (event:any) => {
        const { type, name, value, checked } = event.target;
        // if type is checkbox, then it has attribute checked, otherwise attr. value:
        let updatedValue  = type === 'checkbox' ? checked : value;

        //if input type is number then convert updatedValue string to a number:
        if(type === 'number') {
            updatedValue = +updatedValue;
        }
        //you can put things inside [name] (i.e. numbers, checkboxes) 
        //and they will be "labeled" (sorted) by their name
        const change = { 
            [name] : updatedValue 
        };  

        let updatedProject: Project;

        // functional update because the new project state 
        // is based on the previous project state -
        // it takes the properties from the existing project (...p)
        // and currently existing change object: ...change, adding new change obj.
        setProject((p) => {
            updatedProject = new Project({ ...p, ...change});
            return updatedProject;
        }) 

    }

    return (

            <form className="input-group vertical" onSubmit={ handleSubmit }>
                <label htmlFor= "name">Project name</label>
                <input  value={project.name} 
                        onChange={handleChange} 
                        type="text" 
                        name="name"
                        placeholder="project.name" 
                />
                <label htmlFor="description">Project Description</label>
                <textarea value={project.description}
                          onChange={handleChange} 
                          name="description"
                          placeholder="enter description">
                </textarea>
                <label htmlFor="budget">Project Budget</label>

                <input value={project.budget}
                       onChange={handleChange} 
                       type="number" 
                       name="budget" 
                       placeholder="enter budget" 
                />
                <label htmlFor="isActive">Active?</label>
                <input checked = {project.isActive}
                       onChange={handleChange} 
                       type="checkbox" 
                       name="isActive" 
                />

                <div className="input-group">
                    <button className="primary bordered medium">Save</button>
                    <span></span>
                    <button type="button" className="bordered medium" onClick = {onCancel}>Cancel</button>
                </div>
            </form>

    );
}

export default ProjectForm;